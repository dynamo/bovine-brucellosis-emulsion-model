"""

"""

import csv
from pathlib import Path
import numpy as np
import datetime as dt
import pandas as pd

from emulsion.agent.managers import MultiProcessManager, MetapopProcessManager
from emulsion.agent.atoms import AtomAgent
from emulsion.tools.functions import random_bool


## SUBSTITUTIONS FOR AGE GROUPS IN EVENTS
AGE_GROUP_SUBSTITUTIONS = {
    'YoungerJuvenile': ['OlderJuvenile'],
    'Adult': ['OlderJuvenile'],
    'OlderJuvenile': ['Adult', 'YoungerJuvenile']
}

#===============================================================
# CLASS Metapop (LEVEL 'metapop')
#===============================================================
class Metapop(MetapopProcessManager):
    """
    level of the metapopulation.

    """
    #----------------------------------------------------------------
    # Level initialization
    #----------------------------------------------------------------
    def initialize_level(self, **others):
        """Initialize an instance of Metapop.
        Additional initialization parameters can be introduced here if needed.
        """
        origin = self.model.origin_date
        ## initialize herd structures
        print('Loading herd_structure.csv')
        self.herd_structure = pd.read_csv(self.model.input_dir.joinpath('herd_structure.csv'), sep=',', header=0).fillna(0)
        self.herd_structure['nb_initial_total'] = self.herd_structure['nb_initial_Adult'] + self.herd_structure['nb_initial_OlderJuvenile'] + self.herd_structure['nb_initial_YoungerJuvenile']
        self.herd_structure['step'] = ((pd.to_datetime(self.herd_structure['week_iso'] + "-1", format="%G-W%V-%u") - origin) / self.model.step_duration).round(decimals=0)

        ## initialize events
        print('Loading events.csv')
        self.test_dates = {}
        age_groups = [s.name for s in self.model.state_machines['age_group'].states
                      if not s.autoremove]
        self.moves = {'purch': {}, 'slaught': {}, 'sold':{}, 'death': {}}
        with open(self.model.input_dir.joinpath('events.csv'), 'r') as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=',')
            for row in csvreader:
                herd_id = int(row['herd_id'])
                ## convert ISO weeks into dates then into simulation steps
                date = dt.datetime.strptime(row['week_iso'] + "-1", "%G-W%V-%u")
                step = int(np.round((date - origin) / self.model.step_duration))
                # handle test dates
                do_test = row['test'] and int(row['test'])
                if do_test and self.get_model_value('do_tests'):
                    if herd_id not in self.test_dates:
                        self.test_dates[herd_id] = set()
                    self.test_dates[herd_id].add(step)
                # handle other events
                for event in ['sold', 'purch', 'slaught', 'death']:
                    for age_group in age_groups:
                        qty = row['{}_{}'.format(age_group, event)]
                        if qty:
                            if herd_id not in self.moves[event]:
                                self.moves[event][herd_id] = {}
                            if step not in self.moves[event][herd_id]:
                                self.moves[event][herd_id][step] = {}
                            if age_group not in self.moves[event][herd_id][step]:
                                self.moves[event][herd_id][step][age_group] = 0
                            self.moves[event][herd_id][step][age_group] += int(qty)

        # create initially infected herd
        ## build a new population_id by choosing a herd_id in herd structure
        print('Creating initial infected herd')
        # use specified herd ID if provided
        init_pop_id = int(self.get_model_value('initial_herd_id')) * 1000
        if init_pop_id == 0:
            init_pop_id = self.get_new_population_id(self.get_model_value('introduce_in_dairy_herd'),
                                                     self.get_model_value('initial_min_size'),
                                                     self.get_model_value('initial_max_size'),
                                                     check_sales=True, # to ensure that propagation can occur
                                                     check_purchase=False)
        ## instantiate full prototype of initial herd
        custom_proto = self.customize_herd_prototype('initial_infected_herd', init_pop_id)
        ## instantiate initial herd
        self.initial_herd = self.new_atom(custom_prototype=custom_proto, execute_actions=False)
        self.add_atoms([self.initial_herd])
        self.initial_herd.reapply_prototype(execute_actions=True)
        # initialize additional log file
        ## reset final.txt if first run
        self.final_log_name= Path(self.log_path()).with_name('final.txt')
        if self.final_log_name.exists() and self.statevars.simu_id==0:
            self.final_log_name.unlink()
        ## reset abortions.txt if first run
        self.abortion_log_name= Path(self.log_path()).with_name('abortions.txt')
        if self.abortion_log_name.exists() and self.statevars.simu_id==0:
            self.abortion_log_name.unlink()
        ## reset abortions.txt if first run
        self.abortion_det_log_name= Path(self.log_path()).with_name('abortions-detail.txt')
        if self.abortion_det_log_name.exists() and self.statevars.simu_id==0:
            self.abortion_det_log_name.unlink()

    #----------------------------------------------------------------
    # Specific features
    #----------------------------------------------------------------

    def herds_that_buy_at(self, step):
        """Return the list of herd_id which purchase animals at specified time
        step.

        """
        return [herd_id for herd_id, evts in self.moves['purch'].items() if step in evts]

    def herd_with_sales(self, herd_id):
        """True if the herd_id sells animals, False otherwise.

        """
        return herd_id in self.moves['sold']


    def get_new_population_id(self, herd_type, min_size, max_size, check_sales=False, check_purchase=True):
        """Determine the population_id of a destination herd for animals that
        are sold but first kept in their source herd in quarantine
        until tests "on purchase" are finished.

        Such a population is searched among herd_structure as follows:

        1. All herds available at current time step are taken together
        2. If *herd_type* is not None, herds are selected according
           to their type (dairy/beef). If the resulting list is empty,
           fallback to all herds.
        3. If a min/max size are defined, use them to filter the
           result set. If the resulting list is empty, fallback to
           previous list.
        4. If some of the resulting herds are known for doing purchase
           at current time step, use this subset. Otherwise, fallback
           to previous list.

        """
        # retrieve information of existing herd types
        herds = self.herd_structure
        # filter on current time step
        # print(self.statevars.step, 'filtering herd_structure')
        herds = herds[herds['step'] == self.statevars.step]
        # check size constraints
        if min_size:
            tmp = herds[herds['nb_initial_total'] >= min_size]
            # if any results use this filter, otherwise do not change herds
            if tmp.shape[0] > 0:
                herds = tmp
        if max_size:
            tmp = herds[herds['nb_initial_total'] <= max_size]
            # if any results use this filter, otherwise do not change herds
            if tmp.shape[0] > 0:
                herds = tmp
        # if required in the model, filter by herd type
        if herd_type is not None:
            tmp = herds[herds['dairy_herd'] == herd_type]
            # if any results use this filter, otherwise do not change herds
            if tmp.shape[0] > 0:
                herds = tmp

        available_herds = herds['herd_id'].values

        # if required, check if herds are buying at this time step
        if check_purchase:
            tmp = np.intersect1d(available_herds, np.array(self.herds_that_buy_at(self.statevars.step)))
            # if any results use this filter, otherwise do not change herd_ids
            if any(tmp):
                available_herds = tmp
        # if required, check that the herds performs sales
        if check_sales:
            tmp = [h_id for h_id in available_herds if self.herd_with_sales(h_id)]
            # if any results use this filter, otherwise do not change herd_ids
            if any(tmp):
                available_herds = tmp

        # choose one herd type (herd_id) randomly
        herd_id = np.random.choice(available_herds)
        # build a population_id for the (future) new herd
        return herd_id * 1000 + self.statevars.step


    def customize_herd_prototype(self, base_proto, population_id):
        """Instantiate and modify a concrete prototype ready to be used in
        inititial conditions at level herd.

        """
        # retrieve associated herd type
        herd_id = population_id // 1000
        # retrieve and step at which trade movement was supposed to occur
        trade_step = population_id % 1000
        # iloc[0] necessary below when two lines of the data frame
        # correspond to the same time step (e.g. 2015-W53 and 2016-W01)
        herd = self.herd_structure[(self.herd_structure['herd_id'] == herd_id) &\
                                   (self.herd_structure['step'] == trade_step)].iloc[0]
        proto = self.model.get_prototype('herd', base_proto)
        proto['population_id'] = int(population_id)
        proto['step'] = self.statevars.step
        print(population_id, '->', herd)
        for field in ['herd_id', 'dairy_herd', 'risk', 'nb_initial_YoungerJuvenile', 'nb_initial_OlderJuvenile', 'nb_initial_Adult']:
            proto[field] = float(herd[field])
        return proto

    def create_new_herds(self, pop_ids, source_herd):
        """Create new herds with specified population ID if not already in the
        metapopulation. Created herds keep memory of the *source_herd*
        that was responsible for infecting them.

        """
        new_herds = []
        for new_pop_id in pop_ids:
            if new_pop_id not in self.top_level().get_populations():
                # create instance
                ## instantiate full prototype of dest herd from herd_structure
                custom_proto = self.customize_herd_prototype('default_herd', new_pop_id)
                ## instantiate dest herd
                new_herds.append(self.new_atom(custom_prototype=custom_proto, execute_actions=False))
        # actually move animal to dest herd
        self.add_atoms(new_herds)
        for new_herd in new_herds:
            new_herd.reapply_prototype(execute_actions=True)
            new_herd.statevars.infected_from = source_herd
            new_herd.statevars.step += 1

        print('New herds created:', new_herds)

    #----------------------------------------------------------------
    # Processes
    #----------------------------------------------------------------
    def introduce_infection(self):
        """Introduce infected animal in initial herd after a delay."""
        if self.time == self.get_model_value('delay_before_introduction'):
            proto = 'infected_Adult' if self.get_model_value('introduce_Infectious_Adult') else 'infected_pig_herds'
            qty = int(self.initial_herd.statevars.nb_initial_Infectious)
            infected_animals = [self.initial_herd.new_atom(prototype=proto, execute_actions=False)
                                for _ in range(qty)]
            self.initial_herd.add_atoms(infected_animals)
            for animal in infected_animals:
                animal.reapply_prototype(execute_actions=True)
            print("Introducing infected animals:", qty, proto, "at time", self.time)

    def check_if_finished(self):
        """If metapopulation has flag 'finished', log final state of all herds
        in simulation and deactivate them.

        """
        if self.statevars.finished or self.statevars.step == self.simulation.steps-1:
            for herd in self.get_populations().values():
                herd.log_final_vars()
                herd.deactivate()
            self.deactivate()

#===============================================================
# CLASS Herd (LEVEL 'herd')
#===============================================================
class Herd(MultiProcessManager):
    """
    level of the herd.

    """
    #----------------------------------------------------------------
    # Level initialization
    #----------------------------------------------------------------
    def initialize_level(self, **others):
        """Initialize an instance of Herd.
        """
        self.observed_abortions = dict() ## step -> nb of observed abortions

    #----------------------------------------------------------------
    # Properties
    #----------------------------------------------------------------

    @property
    def number_of_recent_abortions(self):
        """Number of (observed) abortions in the last 4 weeks."""
        step = self.statevars.step
        result = 0
        for week in range(4):
            if (step - week) in self.observed_abortions:
                result += self.observed_abortions[step - week]
        return result

    @property
    def test_scheduled(self):
        """Python function which returns True if the herd is scheduled for
        test at current time step, False otherwise.

        """
        metapop = self.top_level()
        my_id = self.statevars.herd_id
        if my_id not in metapop.test_dates:
            return False
        return self.statevars.step in metapop.test_dates[my_id]

    #----------------------------------------------------------------
    # Additional functions
    #----------------------------------------------------------------
    def substituable_age_groups(self, age_group):
        """Return a list of age groups that can be used for events (death,
        slaughter, sales) when the specified *age_group* is too small for
        providing enough individuals.

        """
        if self.get_model_value('allow_age_group_substitution'):
            return AGE_GROUP_SUBSTITUTIONS[age_group]
        return []


    def log_final_vars(self):
        """Store custom information for current herd (called by the
        metapopulation at the end of the simulation).

        """
        ## logged variables: simu_id, step, population_id, herd_id,
        ## dairy_herd, is_risky,
        ## infected_from, is_APDI, date_APDI_decision_herd,
        ## is_APMS_prophy, is_APMS_abort, is_APMS_intro,
        ## nb_total_abortions, nb_total_declared_abortions, nb_total_abortions_calving_I,
        ## nb_birth,
        ## nb_EAT_ELISA_serologies, nb_FC_serologies,
        ## nb_bulk_milk_tests_total, total_Susceptible,
        ## total_Infectious, total_YoungerJuvenile,
        ## total_OlderJuvenile, total_Adult,
        ## total_YoungerJuvenile_Positive,
        ## total_OlderJuvenile_Positive,
        ## total_Adult_Positive,
        ## total_YoungerJuvenile_Confirmed,
        ## total_OlderJuvenile_Confirmed,
        ## total_Adult_Confirmed,
        self.remove_exited()
        values = [
            self.statevars.simu_id,
            self.statevars.step,
            self.statevars.population_id,
            self.statevars.herd_id,
            self.statevars.dairy_herd,
            self.statevars.is_risky,
            self.statevars.infected_from,
            self.statevars.is_APDI,
            self.statevars.date_APDI_decision_herd,
            self.statevars.is_APMS_prophy,
            self.statevars.is_APMS_abort,
            self.statevars.is_APMS_intro,
            self.nb_total_abortions,
            self.nb_total_declared_abortions,
            self.nb_total_abortions_calving_I,
            self.statevars.nb_birth,
            self.statevars.nb_EAT_ELISA_serologies,
            self.statevars.nb_FC_serologies,
            self.statevars.nb_bulk_milk_tests_total,
            self.total_Susceptible,
            self.total_Infectious,
            self.total_YoungerJuvenile,
            self.total_OlderJuvenile,
            self.total_Adult,
            self.total_YoungerJuvenile_Positive,
            self.total_OlderJuvenile_Positive,
            self.total_Adult_Positive,
            self.total_YoungerJuvenile_Confirmed,
            self.total_OlderJuvenile_Confirmed,
            self.total_Adult_Confirmed
        ]
        message = (','.join(['{}'] * len(values))).format(*values)
        with open(self.upper_level().final_log_name, 'a') as logfile:
            logfile.write('{}\n'.format(message))

        values = [(','.join(['{}']*4)).format(
            self.statevars.simu_id,
            step,
            self.statevars.population_id,
            qty)
                  for step, qty in self.observed_abortions.items()]
        message = '\n'.join([line for line in values])
        with open(self.upper_level().abortion_log_name, 'a') as logfile:
            logfile.write('{}\n'.format(message))


    def move_to_dest_herds(self, to_move):
        """Move animals already sold to their destination herd.

        """
        if any(to_move):
            self.remove_atoms(to_move)
            # gather animals by dest herd
            destinations = {}
            for animal in to_move:
                # retrieve population_id of dest herd
                dest_pop_id = animal.statevars.dest_population_id
                animal.apply_prototype('purchased')
                # keep trace of source herd
                if dest_pop_id not in destinations:
                    destinations[dest_pop_id] = []
                destinations[dest_pop_id].append(animal)
            self.upper_level().create_new_herds(destinations.keys(), self.statevars.population_id)
            for dest_id, animals in destinations.items():
                self.top_level().get_populations()[dest_id].add_atoms(animals)
                # print('Animals', animals, 'moving to new herd', dest_id)


    #----------------------------------------------------------------
    # Processes
    #----------------------------------------------------------------

    def log_custom_vars(self):
        """Store custom minimal information for current herd.

        """
        ## logged variables: simu_id, step, population_id,
        ## total_Susceptible, total_Infectious, total_YoungerJuvenile,
        ## total_OlderJuvenile, total_Adult, total_herd
        self.remove_exited()
        values = [
            self.statevars.simu_id,
            self.statevars.step,
            self.statevars.population_id,
            self.total_Susceptible,
            self.total_Infectious,
            self.total_YoungerJuvenile,
            self.total_OlderJuvenile,
            self.total_Adult,
            self.total_herd,
            self.statevars.is_APMS_abort,
            self.statevars.is_APMS_intro,
            self.statevars.is_APMS_prophy,
        ]
        message = (','.join(['{}'] * len(values))).format(*values)
        with open(self.log_path(), 'a') as logfile:
            logfile.write('{}\n'.format(message))

    def remove_exited(self):
        """Remove all animals marked with 'Exit' age group (culled or sold
        animals). Otherwise they stay in the 'Exit' age group until
        next time step, contributing to potentially dilute the force
        of infection.

        """
        self.make_all_consistent()
        to_remove = self.select_atoms(variable='age_group', state='Exit', process='aging')
        if any(to_remove):
            self.remove_atoms(to_remove)

    def sell_animals(self):
        """Sell animals according to event information stored in
        metapop. Selling healthy animals is equivalent to
        slaughtering. Selling infected animals leads to creating new
        herds to simulate disease spread.

        """
        ## if any animals ready (tested on purchase, with test results
        ## negative), move them to dest herd
        ## These animals are only false-negative infectious (untested
        ## infectious are sent directly to their destination herd,
        ## untested susceptible are just removed, and susceptible
        ## tested negative move to ExitPurchased and are removed too)
        to_move = self.select_atoms(variable='commercial_status', state='Sold', process='commercial_status')
        self.move_to_dest_herds(to_move)

        ## compute animals to sell
        metapop = self.top_level()
        my_id = self.statevars.herd_id
        # check if any slaughtered animals for this herd
        #### trade is allowed only if no Confirmed animal in the herd
        if my_id in metapop.moves['sold'] and not self.get_model_value('is_APMS'):
            step = self.statevars.step
            # check if any slaughtered animals for this herd during this time step
            if step in metapop.moves['sold'][my_id]:
                to_remove = []
                # retrieve dict of quantities per age group
                slaughtered = metapop.moves['sold'][my_id][step]
                for age_group, qty in slaughtered.items():
                    ## determine possible substitutions to specified age_group
                    age_groups = [age_group] + self.substituable_age_groups(age_group)
                    for group in age_groups:
                        # create corresponding animals, assumed healthy
                        candidates = self.select_atoms(variable='age_group',
                                                       state=group,
                                                       process='aging')
                        nb_sold = min(qty, len(candidates))
                        ## back to selling any cows (gestating or not)
                        np.random.shuffle(candidates)
                        # select animals in shuffled lists
                        to_remove += candidates[:nb_sold]
                        qty -= nb_sold
                        # if self.statevars.dairy_herd:
                        #     print('Sold', nb_sold, group)
                        if qty == 0:
                            break

                # print("SOLD:", to_remove)
                infected = []
                for animal in to_remove:
                    if animal.is_Infectious:
                        infected.append(animal)
                    else:
                        animal.apply_prototype('removed')
                ## separate infected in two groups: tested on purchase
                ## by dest herd (stay here in quarantine until tested)
                ## vs. not tested by dest herd (directly move to new
                ## dest herd)
                if infected:
                    nb_to_test = np.random.binomial(len(infected), self.get_model_value('proba_test_on_purchase'))
                    np.random.shuffle(infected)
                    quarantine_for_tests, sold_without_tests = infected[:nb_to_test], infected[nb_to_test:]
                    if sold_without_tests:
                        for animal in sold_without_tests:
                            animal.determine_dest_herd()
                        self.move_to_dest_herds(sold_without_tests)
                    for animal in quarantine_for_tests:
                        animal.apply_prototype('quarantine')

    def check_APDI_decision(self):
        """Declare APDI status and cause total slaughtering of the herd.
        """

        if self.statevars.date_APDI_decision_herd > 0 and self.statevars.step == self.statevars.date_APDI_decision_herd:
            print(self.statevars.population_id, "checking if APDI at t =", self.statevars.step)
            if self.total_Confirmed > 0:
                self.statevars.is_APDI = 1
                self.upper_level().statevars.finished = 1
                print("YES (confirmed case)")
            else:
                print("NO (no confirmed case)")
                self.statevars.date_APDI_decision_herd = 0
                # check if any positive animals
                if self.total_Positive == 0:
                    # if none remove APMS states
                    self.statevars.is_APMS_prophy = 0
                    self.statevars.is_APMS_intro = 0
                    self.statevars.is_APMS_abort = 0
                    print("Exiting APMS")

            ## TODO EMULSION: permettre arret sur condition (ici si 1 herd avec APDI)


    def slaughter_animals(self):
        """Slaughter animals according to event information stored in
        metapop.
        """
        metapop = self.top_level()
        my_id = self.statevars.herd_id
        step = self.statevars.step
        to_remove = []
        allow_slaughter_gestating = self.get_model_value('allow_slaughter_gestating')
        # check if any slaughtered animals for this herd
        if my_id in metapop.moves['slaught']:
            # check if any slaughtered animals for this herd during this time step
            if step in metapop.moves['slaught'][my_id]:
                # retrieve dict of quantities per age group
                slaughtered = metapop.moves['slaught'][my_id][step]
                for age_group, qty in slaughtered.items():
                    ## determine possible substitutions to specified age_group
                    age_groups = [age_group] + self.substituable_age_groups(age_group)
                    for group in age_groups:
                        # create corresponding animals, assumed healthy
                        candidates = self.select_atoms(variable='age_group',
                                                       state=group,
                                                       process='aging')
                        # if allow_slaughter_gestating:
                        #     # take animals randomly (including gestating females)
                        #     np.random.shuffle(candidates)
                        #     nb_removed = min(len(candidates), qty)
                        #     to_remove += candidates[:nb_removed]
                        #     qty -= nb_removed
                        # else:
                        no_gest, gest = [], []
                        for animal in candidates:
                            (gest, no_gest)[animal.is_NoGestation].append(animal)
                        np.random.shuffle(no_gest)
                        np.random.shuffle(gest)
                        if len(no_gest) > 0:
                            nb_removed = min(len(no_gest), qty)
                            to_remove += no_gest[:nb_removed]
                            qty -= nb_removed
                        if qty > 0 and len(gest) > 0 and allow_slaughter_gestating:
                            nb_removed = min(len(gest), qty)
                            to_remove += gest[:nb_removed]
                            qty -= nb_removed
                        # if self.statevars.dairy_herd:
                        #     print('Slaughtered', nb_removed, group)
                        if qty == 0:
                            break
                for animal in to_remove:
                    animal.apply_prototype('removed')

    def mortality_animals(self):
        """Natural mortality of animals according to event information stored in
        metapop.
        """
        metapop = self.top_level()
        my_id = self.statevars.herd_id
        step = self.statevars.step
        to_remove = []
        # check if any slaughtered animals for this herd
        if my_id in metapop.moves['death']:
            # check if any slaughtered animals for this herd during this time step
            if step in metapop.moves['death'][my_id]:
                # retrieve dict of quantities per age group
                dead = metapop.moves['death'][my_id][step]
                for age_group, qty in dead.items():
                    ## determine possible substitutions to specified age_group
                    age_groups = [age_group] + self.substituable_age_groups(age_group)
                    for group in age_groups:
                        # create corresponding animals, assumed healthy
                        candidates = self.select_atoms(variable='age_group',
                                                       state=group,
                                                       process='aging')
                        nb_removed = min(len(candidates), qty)
                        if  nb_removed > 0:
                            np.random.shuffle(candidates)
                            to_remove += candidates[:nb_removed]
                            qty -= nb_removed
                        # if self.statevars.dairy_herd:
                        #     print('Dead', nb_removed, group)
                        if qty == 0:
                            break
                for animal in to_remove:
                    animal.apply_prototype('removed')

    def purchase_animals(self):
        """Purchased animals according to event information stored in metapop."""
        metapop = self.top_level()
        my_id = self.statevars.herd_id
        step = self.statevars.step
        new_animals = []
        # check if any purchase for this herd
        #### trade is allowed only if no APMS status in the herd
        if my_id in metapop.moves['purch'] and not self.get_model_value('is_APMS'):
            # check if any purchase for this herd during this time step
            if step in metapop.moves['purch'][my_id]:
                # retrieve dict of quantities per age group
                purchased = metapop.moves['purch'][my_id][step]
                # print(step, my_id, "has to purchase", purchased)
                for age_group, qty in purchased.items():
                    # create corresponding animals, assumed healthy
                    new_animals += [self.new_atom(prototype='healthy_{}'.format(age_group),
                                                  execute_actions=True)
                                    for _ in range(qty)]
                    # if self.statevars.dairy_herd:
                    #     print('Purchased', qty, age_group)
                # mark animals as purchased with/without test
                if new_animals:
                    nb_to_test = np.random.binomial(len(new_animals), self.get_model_value('proba_test_on_purchase'))
                    np.random.shuffle(new_animals)
                    for i, animal in enumerate(new_animals):
                        # mark animals Dairy/Beef depending on herd type
                        if self.statevars.dairy_herd:
                            animal.apply_prototype('dairy_animal')
                        else:
                            animal.apply_prototype('beef_animal')
                        animal.apply_prototype('purchased' if i >= nb_to_test else 'purchased_tested')

                    # count tests performed on purchase (assumed negative)
                    self.statevars.nb_EAT_ELISA_serologies += nb_to_test
                    self.statevars.nb_FC_serologies += nb_to_test

                    # add animals to herd
                    self.add_atoms(new_animals, init=True)
                # print(my_id, self.statevars.dairy_herd, "="*5+">", new_animals)


    def bulk_milk_test(self):
        """Perform a serological test on bulk milk during annual prophylaxis
        (in dairy herds).

        """
        if self.get_model_value('do_tests') and self.statevars.dairy_herd and self.test_scheduled:
            self.statevars.nb_bulk_milk_tests_total += 1
            self.statevars.nb_bulk_milk_tests_current += 1
            test_result = random_bool(self.get_model_value('proba_detection_bulk_milk'))
            # print('Bulk milk test:', test_result)
            if test_result > 0:
                self.statevars.is_APMS_prophy = float(max(self.statevars.is_APMS_prophy,
                                                          (self.statevars.nb_bulk_milk_tests_current == 2 and self.statevars.is_risky) or (self.statevars.nb_bulk_milk_tests_current == 3)))
                if self.statevars.nb_bulk_milk_tests_current < 3:
                    # schedule new bulk milk tests for confirmation
                    metapop = self.top_level()
                    my_id = self.statevars.herd_id
                    if my_id not in metapop.test_dates:
                        metapop.test_dates = {}
                    delay = self.get_model_value('confirmation_delay' if self.statevars.nb_bulk_milk_tests_current == 1 else 'test_delay')
                    metapop.test_dates[my_id].add(self.statevars.step + delay / self.model.delta_t)
                else:
                    for lactating_cow in self.select_atoms(variable='lactation',
                                                           state='L',
                                                           process='lactation'):
                        lactating_cow.apply_prototype('dairy_positive')
            if self.statevars.nb_bulk_milk_tests_current >= 3 or test_result == 0:
                self.statevars.nb_bulk_milk_tests_current = 0

    def select_animals_to_test(self):
        """Select animals that must be tested in priority during annual
        prophylaxis.
        """
        # nb of animals to test
        if self.get_model_value('do_tests') and not self.statevars.dairy_herd and self.test_scheduled:
            nb_animals = int(self.get_model_value('nb_to_test'))
            # print(nb_animals)
            # priorities of animals to test:
            # 1. animals introduced recently that have been marked "Priority" at purchase
            selection = self.select_atoms(variable='test_status', state='Priority', process='update_test_sample')
            selection = [animal for animal in selection if animal.is_Adult]
            # print("Priority:", len(selection))
            # print("Priority:", sorted([a.agid for a in selection]))
            if len(selection) >= nb_animals:
                # too many animals to test ! select those actually tested and ignore others
                if len(selection) > 0:
                    to_keep = list(np.random.choice(selection, min(len(selection), nb_animals),
                                                    replace=False))
                    # print("To keep:", to_keep)
                    for animal in to_keep:
                        animal.apply_prototype('scheduled_for_test') # ignore animals outside selection
                    selection = to_keep
            else:
                # 2. adults (> 24 months)
                adults = self.select_atoms(variable='age_group', state='Adult', process='aging')
                # 2a. animals never tested
                untested = [animal for animal in adults if animal.is_Untested and not animal.is_ToTest]
                if any(untested):
                    to_test = np.random.choice(untested, min(len(untested), nb_animals-len(selection)),
                                               replace=False)
                    for animal in to_test:
                        animal.apply_prototype('scheduled_for_test')
                    selection += list(to_test)
                    # print("Untested:", len(to_test))
                    # print("Untested:", sorted([a.agid for a in to_test]))
                # 2b. animals previously tested
                if len(selection) < nb_animals:
                    tested = [animal for animal in adults if animal.is_Negative and not animal.is_ToTest]
                    if any(tested):
                        to_test = np.random.choice(tested, min(len(tested), nb_animals-len(selection)),
                                                   replace=False)
                        for animal in to_test:
                            animal.apply_prototype('scheduled_for_test')
                        selection += list(to_test)
                        # print("Negative:", len(to_test))
            # print(len(selection))
            # print("Selection:", sorted([a.agid for a in selection]))
            changed = self.select_atoms(variable='test_status', state='ToTest')
            # print(len(changed))



#===============================================================
# CLASS  (LEVEL 'metapop')
#===============================================================
class Animal(AtomAgent):
    """
    level of the animal.

    """

    # @property
    # def date_APDI_decision_herd(self):
    #     return self.top_level().statevars.date_APDI_decision

    #----------------------------------------------------------------
    # Actions
    #----------------------------------------------------------------
    def set_observed_abortion(self, **others):
        """Memoize the observed abortion to be able to count the number of
        observed abortions in the last 4 weeks.

        """
        step = self.statevars.step
        obs = self.upper_level().observed_abortions
        if step not in obs:
            obs[step] = 1
        else:
            obs[step] += 1

    def log_abortion(self, **others):
        """Log abortion details for this animal.

        """
        ## logged variables: simu_id, step, population_id, nb_recent_abortions, is_Infectious, detected_abortion, declared_abortion
        values = [
            self.statevars.simu_id,
            self.statevars.step,
            self.upper_level().statevars.population_id,
            self.upper_level().number_of_recent_abortions, ## including this one if detected
            int(self.is_Infectious),
            self.statevars.detected_abortion,
            self.statevars.declare_abortion,
        ]
        message = (','.join(['{}'] * len(values))).format(*values)
        with open(self.upper_level().upper_level().abortion_det_log_name, 'a') as logfile:
            logfile.write('{}\n'.format(message))

    def determine_dest_herd(self, *params, **kw):
        """Determine the population_id of a destination herd for animals that
        are sold but first kept in their source herd in quarantine
        until tests "on purchase" are finished.

        """
        # determine the type of destination herd
        if self.get_model_value('trade_by_type'):
            proba_same = self.get_model_value('proba_sell_to_same')
            is_in_dairy_herd = self.upper_level().statevars.dairy_herd
            herd_type = np.random.choice([is_in_dairy_herd, 1-is_in_dairy_herd], p=[proba_same, 1-proba_same])
        else:
            # if no specific trade preferences, use None
            herd_type = None
        # get a new population_id from herd_structure and store it to dest_population_id
        self.statevars.dest_population_id = self.top_level().get_new_population_id(herd_type, self.get_model_value('trade_min_size'), self.get_model_value('trade_max_size'))
